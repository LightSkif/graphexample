﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphExample.Forms
{
    public class DoubleCoordinates
    {
        public double x;
        public double y;

        public DoubleCoordinates(double p1, double p2)
        {
            x = p1;
            y = p2;
        }

        public override bool Equals(Object obj)
        {
            if (obj == this)
                return true;
            if (obj == null || GetType() != obj.GetType())
                return false;

            Coordinates p = (Coordinates)obj;
            return (x == p.x) && (y == p.y);
        }

        public override int GetHashCode()
        {
            double hash = 11;
            hash = 17 * hash + x;
            hash = 17 * hash + y;
            return (int)hash;
        }
    }
}
