﻿// Write your Javascript code.
$(function () {

    trace1 = {
        x: [],
        y: [],
        mode: 'lines+markers',
        name: 'Первый'
    };

    trace2 = {
        x: [],
        y: [],
        mode: 'lines+markers',
        name: 'Второй'
    };    

    $("#graphForm").submit(graphAddition);

    function graphAddition(e) {
        // Отменяем автоматическое обновление страницы при нажатии, вызвавшей функцию кнопки.
        e.preventDefault();

        coord1 = document.getElementById("firstGraphInput").value.trim().split(" ");
        coord2 = document.getElementById("secondGraphInput").value.trim().split(" ");
                
        if (coord1.length % 2 === 0 &&
            coord2.length % 2 === 0) {

            if (isNum(coord1) && isNum(coord2)) {

                if (coord1[0] === coord2[0] &&
                    coord1[coord1.length - 2] === coord2[coord2.length - 2]) {                    

                    postBody = {
                        "first": [],
                        "second": [],
                    }     

                    coordInit(coord1, trace1, postBody.first);
                    coordInit(coord2, trace2, postBody.second);                    

                    postRequest(postBody);
                }
                else {
                    alert("Начальные и конечные координаты по оси абсцисс должны совпадать!");
                }
            }
            else {
                alert("Все значения должны быть целыми числами!");
            }
        }
        else {
            alert("Неверное количество чисел!");
        }        
    }

    // Инициализирует дорожку на графике и часть тела запроса, с помощью введённых данных.
    function coordInit(coord, trace, postSeq) {
        // Очищаем старые значения графика.
        trace.x = [];
        trace.y = [];        
        
        for (var i = 0; i < coord.length; i += 2) {
            postSeq.push({
                "x": coord[i],
                "y": coord[i + 1]
            });

            trace.x.push(coord[i]);
            trace.y.push(coord[i + 1]);
        }
    }

    // Возвращает:
    // true -  массив заполнен только числами.
    // false - массив содержит не число.
    function isNum(seq) {

        for (var i = 0; i < seq.length; i++) {
            if (!(!isNaN(parseFloat(seq[i])) && isFinite(seq[i]))) {
                return false;
            }
        }

        return true;
    }

    // Функция для отправки запроса на сервер.
    function postRequest(postBody) {

        port = location.port;
        baseUrl = location.protocol + "//" + location.hostname + ":" + port + "/graph";

        init = {
            method: "POST",
            headers: {
                'Accept': 'application/json, text/plain, *!/!*',
                'Content-Type': 'application/json'
            }
        };


        init.body = JSON.stringify(postBody);

        fetch(baseUrl, init)
            .then(resp => resp.json()
            .then(data => graphDraw(data)));
    }

    // Вывод полученного с сервера результата.
    function graphDraw(items) {        

        trace3 = {
            x: [],
            y: [],
            mode: 'lines+markers',
            name: 'Результат'
        };

        for (var i = 0; i < items.length; i++) {
            trace3.x.push(items[i].x);
            trace3.y.push(items[i].y);
        }        

        var data = [trace1, trace2, trace3];

        var layout = {
            title: 'Графики'
        };

        Plotly.newPlot("graph", data, layout);
    }
});


