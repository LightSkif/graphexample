﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphExample.Forms
{
    // Форма для получения двух графиков.
    public class GraphInput
    {
        public List<Coordinates> first { get; set; }
        public List<Coordinates> second { get; set; }
    }
}
