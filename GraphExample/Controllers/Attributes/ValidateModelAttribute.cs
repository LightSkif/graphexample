﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace FC2018.Server.Controllers.Attributes
{
    public class ValidateModelAttribute: ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                context.Result = new BadRequestObjectResult(context.ModelState);
                                
                foreach (var key in context.ModelState.Keys)
                {                    
                    foreach (var error in context.ModelState[key].Errors)
                    {                        
                        Console.WriteLine(error.ErrorMessage);
                    }                    
                }
            }                       
        }
    }
}