﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FC2018.Server.Controllers.Attributes;
using Microsoft.AspNetCore.Mvc;
using GraphExample.Forms;

namespace GraphExample.Controllers
{
    [Route("[controller]")]
    [ValidateModel]
    public class GraphController : Controller
    {                
        [HttpPost]        
        public async Task<IActionResult> addition([FromBody] GraphInput form)
        {
            // Графики представлены в виде массива точек.
            // Каждый из графиков состоит из набора ломанных.
            List<Coordinates> firstGraph = form.first;
            List<Coordinates> secondGraph = form.second;

            // Объект для возврата результата сложения графиков.
            List<DoubleCoordinates> resultGraph = new List<DoubleCoordinates>();

            firstGraph.Sort(CompareCoordinates);
            secondGraph.Sort(CompareCoordinates);

            // Проверяем соответствие входных данных основным условиям
            if (firstGraph.Count > 0 &&
                secondGraph.Count > 0 &&
                firstGraph.First().x == secondGraph.First().x &&
                firstGraph.Last().x == secondGraph.Last().x)
            {                
                resultGraph.Add(new DoubleCoordinates(firstGraph[0].x, firstGraph[0].y + secondGraph[0].y));                

                if (firstGraph.Count > 1 && secondGraph.Count > 1)
                {                    
                    // Для сложения графиков каждая точка одного графика проецируется на соответствующую ломанную другого.
                    // Для каждой ломанной вычисляется синус угла между ломанной и осью абсцисс.
                    // Синус используется для вычисления проекции.
                    // Индексы вторых элементов (конец первой ломанной).
                    int firstIndex = 1;
                    int secondIndex = 1;

                    while (firstIndex < firstGraph.Count || secondIndex < secondGraph.Count)
                    {
                        if (firstGraph[firstIndex].x == secondGraph[secondIndex].x)
                        {
                            resultGraph.Add(new DoubleCoordinates(firstGraph[firstIndex].x, firstGraph[firstIndex].y + secondGraph[secondIndex].y));

                            firstIndex++;
                            secondIndex++;
                        }
                        // Строим проекцию точки из второго графика на ломанную из первого.
                        else if (firstGraph[firstIndex].x > secondGraph[secondIndex].x)
                        {
                            // Значение ординат проекции.                            
                            double sin = ((double)(firstGraph[firstIndex].y - firstGraph[firstIndex - 1].y) / (firstGraph[firstIndex].x - firstGraph[firstIndex - 1].x));
                            double yProj = firstGraph[firstIndex - 1].y + ((secondGraph[secondIndex].x - firstGraph[firstIndex - 1].x) * sin);

                            resultGraph.Add(new DoubleCoordinates(secondGraph[secondIndex].x, yProj + secondGraph[secondIndex].y));      
                            secondIndex++;
                        }
                        // Строим проекцию точки из первого графика на ломанную из второго.
                        else if (firstGraph[firstIndex].x < secondGraph[secondIndex].x)
                        {
                            // Значение ординат проекции.
                            double sin = ((double)(secondGraph[secondIndex].y - secondGraph[secondIndex - 1].y) / (secondGraph[secondIndex].x - secondGraph[secondIndex - 1].x));
                            double yProj = secondGraph[secondIndex - 1].y + ((firstGraph[firstIndex].x - secondGraph[secondIndex - 1].x) * sin);

                            resultGraph.Add(new DoubleCoordinates(firstGraph[firstIndex].x, yProj + firstGraph[firstIndex].y));
                            firstIndex++;
                        }
                    }
                }                

                return Ok(resultGraph);
            }
            else
            {
                string errorMessage = "Неверные входные данные";
                ObjectResult result = new ObjectResult(errorMessage);
                result.StatusCode = 409;
                return result;
            }            
        }

        public int CompareCoordinates(Coordinates c1, Coordinates c2)
        {
            if (c1.x > c2.x)
                return 1;
            else if (c1.x < c2.x)
                return -1;
            else
                return 0;
        }
    }    
}