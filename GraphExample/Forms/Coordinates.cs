﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphExample.Forms
{
    public class Coordinates
    {        
        public int x;
        public int y;

        public Coordinates(int p1, int p2)
        {
            x = p1;
            y = p2;
        }

        public override bool Equals(Object obj)
        {
            if (obj == this)
                return true;
            if (obj == null || GetType() != obj.GetType())
                return false;

            Coordinates p = (Coordinates)obj;
            return (x == p.x) && (y == p.y);
        }

        public override int GetHashCode()
        {
            return x ^ y;
        }        
    }
}
